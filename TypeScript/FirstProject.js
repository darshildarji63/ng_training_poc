// Basic Hello World Project
var message = "Hello Darshil";
var message1 = "Good Evening"; // Explicitly defined type
// message = 101;          error: Type 'number' is not assignable to type 'string'.
console.log(message + " - " + message1);
// Defining Type
var user1 = {
    name: "Darshil",
    id: 1
};
var user = {
    name: "Darshil Darji",
    id: 2
};
console.log(user.id + " " + user.name);
var isTrue = "open";
console.log(typeof isTrue);
//********** Interface **********
// Def: One of TypeScript’s core principles is that type checking focuses on the shape that values have. This is sometimes called “duck typing” or “structural subtyping”. In TypeScript, interfaces fill the role of naming these types, and are a powerful way of defining contracts within your code as well as contracts with code outside of your project.
// ---------- First Interface using basic steps ----------
// function has a single parameter that requires that the object passed in has a property called label of type string.
function printLabel(labeledObj) {
    //parsed interface object with it's shape
    console.log(labeledObj.label);
}
var myObj = { size: 10, label: "Size 10 Object" }; //compiler only checks that at least the ones required are present
printLabel(myObj);
function printLabel1(labeledObj) {
    //labeledobj is a variable of the type LabeledValue interface
    console.log(labeledObj.label);
}
var myObj1 = { size: 10, label: "Size 10 object from interface" };
printLabel1(myObj1);
