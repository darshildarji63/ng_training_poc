// Basic Hello World Project
let message = "Hello Darshil";
let message1: string = "Good Evening"; // Explicitly defined type

// message = 101;          error: Type 'number' is not assignable to type 'string'.
console.log(message + " - " + message1);

// Defining Type
const user1 = {
  name: "Darshil",
  id: 1,
};

//Use Interface to explicitly describe the shape of object's
interface User {
  name: string;
  id: number;
}

const user: User = {
  name: "Darshil Darji",
  id: 2,
};

console.log(user.id + " " + user.name);

// Composing Types
// Unions :  With a union, you can declare that a type could be one of many types. For example, you can describe a boolean type as being either true or false:
// type myBool = true | false;
type myBool = "open" | "close";
let isTrue: myBool = "open";
console.log(typeof isTrue);

//********** Interface **********
// Def: One of TypeScript’s core principles is that type checking focuses on the shape that values have. This is sometimes called “duck typing” or “structural subtyping”. In TypeScript, interfaces fill the role of naming these types, and are a powerful way of defining contracts within your code as well as contracts with code outside of your project.

// ---------- First Interface using basic steps ----------
// function has a single parameter that requires that the object passed in has a property called label of type string.
function printLabel(labeledObj: { label: string }) {
  //parsed interface object with it's shape
  console.log(labeledObj.label);
}
let myObj = { size: 10, label: "Size 10 Object" }; //compiler only checks that at least the ones required are present
printLabel(myObj);

// ---------- First Interface using an interface to describe the requirement of having the label property that is a string ----------
interface LabeledValue {
  label: string;
}
function printLabel1(labeledObj: LabeledValue) {
  //labeledobj is a variable of the type LabeledValue interface
  console.log(labeledObj.label);
}

let myObj1 = { size: 10, label: "Size 10 object from interface" };
printLabel1(myObj1);
