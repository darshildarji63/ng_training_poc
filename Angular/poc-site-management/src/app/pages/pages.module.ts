// This will be child module which is under main App module. Reason behind creating this module is to reduce load on main module and it will be easy to use in further development
// This module also called as featured module
// CLI: ng generate module moduleName --routing This will generate module with routing
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { UserComponent } from './user/user.component';
import { PagesComponent } from './pages.component';

@NgModule({
  declarations: [PagesComponent, UserComponent],
  imports: [CommonModule, PagesRoutingModule],
})
export class PagesModule {}
