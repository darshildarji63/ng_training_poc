//Generated this component using CLI code: ng generate/g component pages/user which will generate component into specific module if we have multiple feature modules
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'user-component',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass'],
})
export class UserComponent implements OnInit {
  userList: any = [
    {
      id: 1,
      name: 'Darshil',
    },
  ];
  constructor() {}
  ngOnInit(): void {}
}
