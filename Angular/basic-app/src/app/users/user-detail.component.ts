import { Component, OnInit } from '@angular/core';
import { IUser } from './user';

@Component({
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss'],
})
export class UserDetailComponent implements OnInit {
  pageTitle: string = 'User Detail';
  user: IUser;
  constructor() {}

  ngOnInit(): void {}
}
