import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { IUser } from './user';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root', //Register service in root so it will be available for every component in the angular application
})
export class UserService {
  private userURL = 'api/users.json';

  constructor(private http: HttpClient) {}

  getUsers(): Observable<IUser[]> {
    return this.http.get<IUser[]>(this.userURL).pipe(
      tap((data) => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
    // return [
    //   {
    //     id: 1,
    //     firstName: 'Darshil',
    //     lastName: 'Darji',
    //     siteName: 'site Number1',
    //     defaultVal: 'Darshil Darji',
    //     imageUrl: 'assets/images/profile1.png',
    //     userRating: 3.2,
    //   },
    //   {
    //     id: 2,
    //     firstName: 'Vipul',
    //     lastName: 'Pagrut',
    //     siteName: 'site Number2',
    //     defaultVal: 'Vipul Pagrut',
    //     imageUrl: 'assets/images/profile2.png',
    //     userRating: 4.2,
    //   },
    //   {
    //     id: 3,
    //     firstName: 'Javed',
    //     lastName: 'Shaikh',
    //     siteName: 'site Number3',
    //     defaultVal: 'Javed Shaikh',
    //     imageUrl: 'assets/images/profile2.png',
    //     userRating: 4.8,
    //   },
    // ];
  }
  private handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occured: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
