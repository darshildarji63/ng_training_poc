import { Component, OnInit } from '@angular/core';
import { IUser } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'sm-users',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  pageTitle: string = 'User List';
  imageWidth: number = 50;
  imageMargin: number = 2;
  showImage: boolean = false;
  errorMessage: string = '';
  // filterValue: string = 'firstName';

  _filterValue: string;
  get filterValue(): string {
    return this._filterValue;
  }
  set filterValue(value: string) {
    this._filterValue = value;
    this.filteredUsers = this.filterValue
      ? this.performFilter(this.filterValue)
      : this.users;
  }

  filteredUsers: IUser[];
  users: IUser[] = [];
  // users: IUser[] = [
  //   {
  //     id: 1,
  //     firstName: 'Darshil',
  //     lastName: 'Darji',
  //     siteName: 'site Number1',
  //     defaultVal: 'Darshil Darji',
  //     imageUrl: 'assets/images/profile1.png',
  //     userRating: 3.2,
  //   },
  //   {
  //     id: 2,
  //     firstName: 'Vipul',
  //     lastName: 'Pagrut',
  //     siteName: 'site Number2',
  //     defaultVal: 'Vipul Pagrut',
  //     imageUrl: 'assets/images/profile2.png',
  //     userRating: 4.2,
  //   },
  // ];

  constructor(private userService: UserService) {
    this._filterValue = 'darshil';
    this.filterValue = 'darshil';
  }
  onRatingClicked(message: string): void {
    this.pageTitle = 'User List: ' + message;
  }
  performFilter(filterBy: string) {
    filterBy = filterBy.toLocaleLowerCase();
    return this.users.filter(
      (user: IUser) => user.firstName.toLocaleLowerCase().indexOf(filterBy) != 1
    );
  }
  toggleProfileImage(): void {
    this.showImage = !this.showImage;
  }
  ngOnInit(): void {
    console.log('In OnInit');
    this.userService.getUsers().subscribe({
      // to make arrow function multiline user curly braces like below
      next: (users) => {
        this.users = users;
        this.filteredUsers = this.users;
      },
      error: (err) => (this.errorMessage = err),
    });
    // this.users = this.userService.getUsers();
    // this.filteredUsers = this.users;
  }
}
