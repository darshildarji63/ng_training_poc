export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  siteName: string;
  defaultVal: string;
  imageUrl: string;
  userRating: number;
}
