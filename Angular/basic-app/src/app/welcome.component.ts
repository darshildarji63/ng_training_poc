import { Component } from '@angular/core';

@Component({
  template: `<div class="card">
    <div class="card-header">
      <h4>Welcome Component</h4>
    </div>
    <div class="card-body">Select your option from topbar</div>
  </div>`,
})
export class WelcomeComponent {}
