import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';

@Component({
  selector: 'sm-star',
  templateUrl: './star.component.html',
  styleUrls: ['./star.component.scss'],
})
export class StarComponent implements OnChanges {
  starWidth: number;
  @Input() rating: number;
  @Output() ratingClicked: EventEmitter<string> =
        new EventEmitter<string>();

  ngOnChanges() {
    this.starWidth = this.rating * 65 / 5;
  }

  onClick(): void{
    console.log(`The user rating ${this.rating} was clicked`);  //used ES2015 backtick to define a template string. This allow us to embed the rating directly into this string
    this.ratingClicked.emit(`The user rating ${this.rating} was clicked`);
  }
}
